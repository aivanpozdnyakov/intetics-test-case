# Child play django, WOW
How to run:
```
sudo apt-get install postgresql-client
pip3 install gunicorn
pip3 install django-admin-sortable2
sudo service postgresql start
python3 manage.py makemigrations sorter
python3 manage.py migrate
gunicorn --bind localhost:8000 mysite.wsgi
```