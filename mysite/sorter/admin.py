from django.contrib import admin
from .models import SortInfo
# from adminsortable2.admin import SortableAdminMixin

# def sort_by_sort_type(modeladmin, request, queryset):
#     modeladmin.ordering = ('sort_type', )

# sort_by_sort_type.short_description = "Sort by algorithm type"


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('sort_type', 'pub_date', 'time_passed', 'length')
    ordering = ('pub_date', 'sort_type', 'time_passed', 'length')
    list_filter = ('sort_type',)
    search_fields = ('sort_type', 'pub_date', 'time_passed', 'length')
    # actions = (sort_by_sort_type, )


admin.site.register(SortInfo, ArticleAdmin)

# Register your models here.
