from abc import ABCMeta, abstractmethod
from random import randint
import datetime
from functools import wraps


class SorterAbstactClass(metaclass=ABCMeta):
    def __init__(self):
        self.array = []

    def set_random_array(self, number=20, range_from=-10, range_to=10):
        self.array = [randint(range_from, range_to) for _ in range(number)]

    def set_array(self, array):
        self.array = array.copy()

    @abstractmethod
    def sort(self):
        pass


def time_decorator(func):
    @wraps(func)
    def to_return(self, *args, **kwargs):
        start = datetime.datetime.now()
        res = func(self, *args, **kwargs)
        end = datetime.datetime.now()
        elapsed = end - start
        return res, elapsed

    return to_return


class BubbleSort(SorterAbstactClass):
    @time_decorator
    def sort(self):
        n = len(self.array)
        for i in range(n - 1):
            for j in range(n - i - 1):
                if self.array[j] > self.array[j + 1]:
                    self.array[j], self.array[j + 1] = self.array[j + 1], self.array[j]
        return self.array


class InsertionSort(SorterAbstactClass):
    @staticmethod
    def insertion_sort(array):
        i = 1
        while i < len(array):
            x = array[i]
            j = i - 1
            while j >= 0 and array[j] > x:
                array[j + 1] = array[j]
                j -= 1
            array[j + 1] = x
            i += 1
        return array

    @time_decorator
    def sort(self):
        return self.insertion_sort(self.array)



class TimSort(SorterAbstactClass):
    @time_decorator
    def sort(self):
        return sorted(self.array)

class MergeSort(SorterAbstactClass):
    @staticmethod
    def merge(left, right):
        result = []
        i, j = 0, 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1
        while i < len(left):
            result.append(left[i])
            i += 1
        while j < len(right):
            result.append(right[j])
            j += 1
        return result

    @staticmethod
    def merge_sort(array):
        if len(array) == 1:
            return array
        else:
            middle = int(len(array) / 2)
            left = MergeSort.merge_sort(array[:middle])
            right = MergeSort.merge_sort(array[middle:])
            return MergeSort.merge(left, right)

    @time_decorator
    def sort(self):
        return self.merge_sort(self.array)

class QuickSort(SorterAbstactClass):
    @staticmethod
    def quicksort(arr, k=0, start=0, end=None):
        if end is None:
            end = len(arr) - 1
        if end <= start:
            return
        if len(arr) <= k:
            InsertionSort.insertion_sort(arr)
            return
        div_ind = (start + end) // 2
        divider = arr[div_ind]
        i, j = start, end
        while i <= j:
            while arr[i] < divider:
                i += 1
            while arr[j] > divider:
                j -= 1
            if i <= j:
                arr[i], arr[j] = arr[j], arr[i]
                i += 1
                j -= 1
        QuickSort.quicksort(arr, k, start, j)
        QuickSort.quicksort(arr, k, i, end)
        return arr

    @time_decorator
    def sort(self):
        return self.quicksort(self.array, k=14)