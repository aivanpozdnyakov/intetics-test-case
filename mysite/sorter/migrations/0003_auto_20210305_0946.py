# Generated by Django 3.1.7 on 2021-03-05 09:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sorter', '0002_auto_20210305_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='sortinfo',
            name='length',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sortinfo',
            name='pub_date',
            field=models.DateTimeField(),
        ),
    ]
