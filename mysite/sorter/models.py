from django.db import models
from datetime import timedelta


class SortInfo(models.Model):
    sort_type = models.CharField(max_length=200)
    pub_date = models.DateTimeField()
    time_passed = models.DurationField(default=timedelta())
    length = models.IntegerField()

    def __str__(self):
        return f"{self.sort_type} within {self.time_passed}"


# Create your models here.
