from django.urls import path

from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # ex: /polls/
    path('', views.index, name='index'),
    path('sort', views.sort, name='sort')
]

urlpatterns += staticfiles_urlpatterns()
