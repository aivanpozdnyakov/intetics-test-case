from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .algorithm.algorithm import BubbleSort, SorterAbstactClass
from django import forms
from .models import SortInfo
from django.utils import timezone
workers = SorterAbstactClass.__subclasses__()
sorters_list = [x.__name__ for x in workers]


def index(request):
    return render(request, 'sorter/index.html', {
        'sorters': sorters_list,
        'error': request.POST.get('error', '')
    })


def create_array_from_file(f):
    splited = f.read().decode("utf-8").split()
    return [float(x) for x in splited]


def sort(request):
    try:
        original_array = create_array_from_file(request.FILES['array_file'])
        Worker = workers[int(request.POST['worker']) - 1]
        worker = Worker()
        worker.set_array(original_array)
        sorted_array, time_passed = worker.sort()
        s = SortInfo(sort_type=Worker.__name__,
                     pub_date=timezone.now(),
                     time_passed=time_passed,
                     length=len(original_array))
        s.save()
        return render(
            request, 'sorter/sorted.html', {
                'original_array': original_array,
                'sorted_array': sorted_array,
                'time_passed': time_passed
            })
    except Exception as e:
        print(e)
        return render(request, 'sorter/sorted.html', {'error': True})
